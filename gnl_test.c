/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   gnl_test.c                                         :+:    :+:            */
/*                                                     +:+                    */
/*   By: tjans <tjans@student.codam.nl>               +#+                     */
/*                                                   +#+                      */
/*   Created: 2019/11/18 16:53:41 by tjans         #+#    #+#                 */
/*   Updated: 2019/11/26 19:23:39 by tjans         ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include <criterion/criterion.h>
#include <criterion/parameterized.h>
#include <criterion/hooks.h>
#include <string.h>
#include <fcntl.h>
#include <stdio.h>

int	get_next_line(int, char**);

Test(gnl_test, invalid_input)
{
	char *str;

	cr_assert(get_next_line(-1337, &str) == -1, "no error fd < 0");
	cr_assert(get_next_line(42, &str) == -1, "no error unused fd");
	cr_assert(get_next_line(open("noexist", O_RDONLY), &str) == -1, "no error nonexistient file");
	cr_assert(get_next_line(0, NULL) == -1, "no error str ptr NULL");
}

ParameterizedTestParameters(gnl_test, basic_files)
{
	char **params;
	const size_t nb_params = 4;
	size_t i;

	i = 0;
	params = cr_malloc(sizeof(char*) * 4);
	while (i < nb_params)
	{
		params[i] = cr_calloc(32, 1);
		i++;
	}
	strncpy(params[0], "basic/empty.txt", 32);
	strncpy(params[1], "basic/nonl.txt", 32);
	strncpy(params[2], "basic/full.txt", 32);
	strncpy(params[3], "basic/emptyline.txt", 32);
	return (cr_make_param_array(char*, params, nb_params));
}

ParameterizedTest(char **param, gnl_test, basic_files)
{
	size_t	lines;
	int		ret;
	FILE	*fs;
	int		fd;
	char	str[8196];
	char	*str2;

	lines = 0;
	ret = 1;
	fs = fopen(*param, "r");
	fd = open(*param, O_RDONLY);
	cr_assert(fs, "Cannot open test file %s", *param);
	cr_assert(fd > 0, "Cannot open test file %s", *param);
	str[0] = 0;
	while (fgets(str, 8196, fs))
	{
		cr_assert(ret != 0, "gnl returned 0 while there were more lines to read");
		lines++;
		ret = get_next_line(fd, &str2);
		if (str[strlen(str) - 1] == '\n')
			str[strlen(str) - 1] = '\0';
		cr_expect_str_eq(str2, str, "line mismatch\ngnl:\t%s\nfgets:\t%s\n%ld, %ld\n", str2, str, strlen(str2), strlen(str));
	}
	ret = get_next_line(fd, &str2);
	if (!lines)
		ret = get_next_line(fd, &str2);
	cr_assert(ret == 0, "EOF reached, gnl returns 1\nFILE: %s lines read: %ld\nLast line: %s\n", *param, lines, str);
	printf("file: %s lines: %ld\n", *param, lines);
}

#define START_A 4
#define LINES_A 70
#define LINES_B 17

Test(gnl_test, interleaved_files)
{
	int		fd1,fd2,fd3, ret;
	FILE	*fs;
	char	str1[8196];
	char	*str2;
	size_t	lines;

	ret = 1;
	fd1 = open("il/layout.txt", O_RDONLY);
	fd2 = open("il/a.txt", O_RDONLY);
	fd3 = open("il/b.txt", O_RDONLY);
	fs = fopen("il/expected.txt", "r");
	lines = 0;
	cr_assert(
			fd1 > 0 &&
			fd2 > 0 &&
			fd3 > 0 &&
			fs,
			"Error while opening one of the test files");
	while (fgets(str1, 8196, fs))
	{
		cr_assert(ret != 0, "gnl returned 0 while there were more lines to read");
		lines++;
		if (lines < START_A)
			ret = get_next_line(fd1, &str2);
		else if (lines < START_A + LINES_A)
			ret = get_next_line(fd2, &str2);
		else if (lines < START_A + LINES_A + 2)
			ret = get_next_line(fd1, &str2);
		else if (lines < START_A + LINES_A + 2 + LINES_B)
			ret = get_next_line(fd3, &str2);
		else
			ret = get_next_line(fd1, &str2);
		if (ret == 0 && (
					lines == LINES_A + START_A - 1 ||
					lines == START_A + LINES_A + LINES_B + 1))
			ret = 1;
		if (str1[strlen(str1) - 1] == '\n')
			str1[strlen(str1) - 1] = '\0';
		cr_expect_str_eq(str1, str2, "\nline mismatch at %ld\nexpected:\t%s\nactual:\t%s\n", lines, str1, str2);
	}
}

Test(gnl_test, stdin_pipe,
		.description = "Testing reading from stdin (pipe)")
{
	FILE	*ps;
	FILE	*gs;
	char	str1[8196];
	char	str2[8196];

	ps = popen("man man", "r");
	cr_assert(ps, "Cannot execute man");
#ifndef BONUS
	gs = popen("man man | ./test_stdin", "r");
#else
	gs = popen("man man | ./test_stdin_bonus", "r");
#endif
	cr_assert(gs, "Cannot execute test binary");
	while (fgets(str1, 8196, ps) && fgets(str2, 8196, gs))
		cr_expect_str_eq(str1, str2, "stdin line mismatch\ngnl:\t%s\nman:\t%s\n", str2, str1);
	cr_expect_null(fgets(str1, 8196, ps), "gnl didn't read everything");
	pclose(ps);
	pclose(gs);
}

Test(gnl_test, stdin_siminput,
		.description = "Simulating shell input")
{
	printf("Sadly i didn't automate testing manual input yet\n%s",
			"use ./test_stdin\n");
}
