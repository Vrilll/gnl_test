#!/bin/bash
# **************************************************************************** #
#                                                                              #
#                                                         ::::::::             #
#    test_all.sh                                        :+:    :+:             #
#                                                      +:+                     #
#    By: tjans <tjans@student.codam.nl>               +#+                      #
#                                                    +#+                       #
#    Created: 2019/11/19 20:41:57 by tjans         #+#    #+#                  #
#    Updated: 2019/11/19 20:41:57 by tjans         ########   odam.nl          #
#                                                                              #
# **************************************************************************** #

make
if [ "$1" = "bonus" ]; then
	./test_bonus -j1 --verbose --xml=test-results/bonus_$BUFFER_SIZE.xml
else
	./test_gnl -j1 --verbose --xml=test-results/gnl_$BUFFER_SIZE.xml
fi;
make clean
