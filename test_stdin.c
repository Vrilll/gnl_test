/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   test_stdin.c                                       :+:    :+:            */
/*                                                     +:+                    */
/*   By: tjans <tjans@student.codam.nl>               +#+                     */
/*                                                   +#+                      */
/*   Created: 2019/11/19 17:10:56 by tjans         #+#    #+#                 */
/*   Updated: 2019/11/19 17:24:51 by tjans         ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>

int	get_next_line(int, char**);

int main(void)
{
	char	*line;
	int		ret;

	ret = 1;
	while (ret)
	{
		ret = get_next_line(0, &line);
		printf("%s\n", line);
	}
	return (0);
}
